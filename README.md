1. Clone repository
2. Modify the django settings file. Both the 'STATICFILES_DIR' and the 'STATIC_ROOT' variables require absolute paths. 
3. Run the 'collectstatic' command (i.e. python manage.py collectstatic)
4. Run the 'syncdb' command (i.e. python manage.py syncdb)
5. Run the server (i.e. python manage.py runserver)
6. Open browser to localhost on the correct port


