# Create your views here.

from django.http import HttpResponse
from django.shortcuts import render

from django.contrib.auth.models import User
from addressBookApp.serializers import *
from addressBookApp.models import AddressBook

from rest_framework import viewsets
from rest_framework.response import Response

class AddressBookViewset(viewsets.ModelViewSet):
    queryset = AddressBook.objects.all()
    serializer_class = AddressBookSerializer

    def pre_save(self, obj):
    	obj.user = User.objects.get(pk=1)

class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

def hello(request):
    return HttpResponse('hello')

def main(request):
	ctx = dict()
	return render(request, 'addressBookApp/app/index.html', ctx)



