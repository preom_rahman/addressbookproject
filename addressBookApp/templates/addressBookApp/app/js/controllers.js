'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MyCtrl1', [function() {

  }])
  .controller('MyCtrl2', [function() {

  }])
  .controller('homeCtrl', ['$scope', '$http', function($scope, $http) {

    // Remember the trailing slash!
    var mainUrl = 'rest/address/';

  	$scope.addresses = [];

  	var getAddresses = function(){
	  	$http.get(mainUrl).success(function(data, responseHeaders){
	  		//console.log(data);
	  		$scope.addresses = data;
	  	});
  	}

  	$scope.addAddress = function(){
  		var data = {
  			name: $scope.name,
  			postal_code: $scope.postal_code
  		};

  		$http.post(mainUrl, data).success(function(data, responseHeaders){
  			getAddresses();
  		});
  	}

    $scope.updateItem = function(index){
      var address = $scope.addresses[index];
      var data = {
        name: address.name,
        postal_code: address.postal_code,
        isHomeAddress: address.isHomeAddress
      };

      var url = mainUrl + index + '/';
      var url = 'rest/address/' + address.id + '/';

      console.log(url, data);

      $http.put(url, data).success(function(data, responseHeaders){
        console.log(data);
        getAddresses();
      });
    }

  	$scope.deleteItem = function(index){
      var address = $scope.addresses[index];
  		var url = mainUrl + address.id + '/';
  		console.log(url);
  		$http.delete(url).success(function(data, responseHeaders){
  			getAddresses();
  		});
  	}

    console.log('Start App...');
    getAddresses();

  }]);