from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class AddressBook(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=100)
    isHomeAddress = models.BooleanField(default=True)

