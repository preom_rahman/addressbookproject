from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from addressBookApp import views
from rest_framework.routers import DefaultRouter

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

router = DefaultRouter()
router.register(r'address', views.AddressBookViewset)
router.register(r'users', views.UserViewset)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'addressBookProject.views.home', name='home'),
    # url(r'^addressBookProject/', include('addressBookProject.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^test', 'addressBookApp.views.hello'),
    url(r'^rest/', include(router.urls)),
    url(r'^', 'addressBookApp.views.main'),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))

)

urlpatterns += staticfiles_urlpatterns()
