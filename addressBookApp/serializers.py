from django.contrib.auth.models import User
from rest_framework import serializers
from addressBookApp.models import AddressBook

class AddressBookSerializer(serializers.ModelSerializer):
    user = serializers.Field(source='user.username')
    class Meta:
        model = AddressBook
        fields = ('user', 'name', 'postal_code', 'isHomeAddress', 'id')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', )

