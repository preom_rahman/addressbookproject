'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MyCtrl1', [function() {

  }])
  .controller('MyCtrl2', [function() {

  }])
  .controller('homeCtrl', ['$scope', '$http', function($scope, $http) {
  	$scope.addresses = [];
  	var getAddresses = function(){
	  	$http.get('/rest/address/').success(function(data, responseHeaders){
	  		console.log(data);
	  		$scope.addresses = data;
	  	});
  	}

  	$scope.addAddress = function(){
  		var data = {
  			name: $scope.name,
  			postal_code: $scope.postal_code
  		};

  		$http.post('/rest/address/', data).success(function(data, responseHeaders){
  			getAddresses();
  		});
  	}

  	$scope.deleteItem = function(index){
  		var url = '/rest/address/' + $scope.addresses[index].id + '/';
  		console.log(url);
  		$http.delete(url).success(function(data, responseHeaders){
  			getAddresses();
  		});
  	}

    console.log('Start App...');
    getAddresses();

  }]);